<!doctype html>
<html ng-app="yNewsApp">
<head>

<script type="text/javascript">
    
// GLOBAL CONFIG
var API_URL = "../api/index.php/";
var FB_APP_ID = "1507979672750288"; 

</script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-route.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.5.8/angular-sanitize.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/ynews.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">

    <script type="text/javascript" src="js/angulike.js"></script>
    <script type="text/javascript" src="js/starRating.js"></script>
    <script type="text/javascript" src="js/searchSelection2.js"></script>
    <script type="text/javascript" src="js/btLoading.js"></script>

    <script src="js/yNewsApp.js"></script>

    <title>Y-news</title>
</head>
<body>



<nav class="navbar">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span class="navbar-brand" href="#">Y-news</span>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a ng-class="{ active: isCategory('all') }" href="#/">Home</a></li>
                <li><a ng-class="{ active: isCategory('science') }" href="#category/science">Science</a></li>
                <li><a ng-class="{ active : isCategory('tech') }" href="#category/tech">Tech</a></li>
                <li><a ng-class="{ active: isCategory('world') }" href="#category/world">World</a></li>
                <li><a ng-class="{ active: isCategory('politics') }" href="#category/politics">Politics</a></li>
                <li><a ng-class="{ active: isCategory('health') }" href="#category/health">Health</a></li>
                <li><a ng-class="{ active: isPage('contact') }" href="#contact">Contact</a></li>
            </ul>
            <form class="navbar-form navbar-right" role="search">
                <div class="input-group add-on">
                    <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term" ng-model="searchTerm">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit" ng-click="articleSearch();">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<!--- LOADING -->
<bt-loading show-loading="rootShowLoading"></bt-loading>
<? /*
<div class="popup-container" ng-show="rootShowLoading || root_msg.show">
    <div class="bg" ng-show="rootShowLoading">
        <p>Loading...</p>
        <p><img src="images/loading.gif"></p>
    </div>
    <div ng-show="root_msg.show" class="alert alert-{{ root_msg.type }}">
        <h3>{{ root_msg.msg }}</h3>
        <p class="small text-right">
            <a ng-click="hide_message()">
                Dismiss
            </a>
        </p>
    </div>
</div>
<!-- END -->
*/ ?>

<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>{{ pageTitle }} </h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<ng-view></ng-view>

<section id="footer">
    <div class="container ">
        <div class="row footer-box">
            <div class="col-md-6">
                Copyleft &copy; 2016 by <b>Y-news</b>. All Rights Reserved.
            </div>
            <div class="col-md-6 text-right">
                <a href="#">Home</a>
                <a ng-class="{ active: isPage('about') }" href="#about">About Us</a>
                <a ng-class="{ active: isPage('contact') }" href="#contact">Contact</a>
            </div>
        </div>
    </div>
</section>


</body>
</html>



<script type="text/ng-template" id="categoryView.html">
    <section class="news-list">
        <div class="container">
            <div class="row">
                <div class="col-md-6 article-box" ng-repeat="a in articles">
                    <div class="top">
                        <div class="row">
                            <div class="col-sm-4">
                                <p>
                                    {{ a.date }}<br>
                                    Views: {{ a.views }}
                                </p>
                            </div>
                            <div class="col-sm-4">
                                <star-rating ng-model="a.my_rating"
                                             max="5"
                                             readonly="a.my_rating > 0"
                                             on-rating-select="addRating(rating, a.id)"></star-rating>
                                <br />
                                Average: {{a.rating || 0}}
                            </div>
                            <div class="col-sm-4 text-right">
                                <div fb-like="a.url"></div>
                            </div>
                        </div>
                    </div>
                    <div class="description">
                        <a target="_blank" class="title" ng-click="open(a.id)">{{ a.title }}</a>
                        <div ng-bind-html="a.description" class="{{a.id}}"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <ul class="pagination pagination-lg">
                        <li><a ng-click="nextPage(-1)" class="first">Previous</a></li>
                        <li ng-repeat="i in pages" class="{{ pageNum == i ? 'active' : '' }}">
                            <a href="{{ pageLink(i) }}">{{ i }}</a>
                        </li>
                        <li><a ng-click="nextPage()" class="last">Next</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</script><!-- ng template -->

<script type="text/ng-template" id="contactView.html">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <form>
                        <p>
                            <input type="text" name="" class="form-control" placeholder="Your e-mail" ng-model="email">
                        </p>
                        <p>
                            <textarea class="form-control" ng-model="msg" placeholder="Your message"></textarea>
                        </p>
                        <p>
                            <button class="btn btn-lg btn-default" ng-click="sendMessage();">Send message</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </section>
</script><!-- ng template -->

<script type="text/ng-template" id="aboutView.html">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-left">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut purus massa. Duis semper turpis neque, quis gravida diam aliquam ac. Quisque metus ipsum, placerat sed gravida id, scelerisque eget arcu. Donec augue diam, feugiat quis euismod vitae, pretium feugiat sem. Nulla pretium velit in lacinia imperdiet. Integer varius metus in mauris tempus pulvinar. Vestibulum at lobortis nunc, quis viverra erat. In a fringilla libero, sagittis vulputate dui. Sed venenatis sed libero a facilisis. Ut venenatis accumsan arcu, non luctus orci tristique at. Morbi sagittis felis et tincidunt pellentesque.
                    </p>
                    <p>
                        Morbi quis tincidunt ex, sit amet consectetur elit. Sed magna lacus, luctus ac semper non, rhoncus non erat. Mauris sagittis neque eu velit efficitur, suscipit iaculis dolor hendrerit. Cras euismod sapien ac nisi faucibus, sed dignissim velit sagittis. Nam et euismod risus, et fermentum nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla feugiat porta sem, quis pulvinar neque. Mauris pellentesque nulla augue, nec dictum arcu molestie congue. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec sit amet risus congue, dignissim urna ac, elementum ex. Etiam luctus lorem vitae diam venenatis consectetur.                   
                    </p>
                </div>
            </div>
        </div>
    </section>
</script><!-- ng template -->