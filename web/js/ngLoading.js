"use strict";

(function () {

angular.module('btLoading', []).directive('btLoading', function() {
	return {
		restrict: 'EA',// < attribute and element directive
		template:
// removed readonly class...
//			'<ul class="star-rating" ng-class="{readonly: readonly}">' +
			'<div class="popup-container" ng-show="showLoading">'
				+ '<div class="bg" ng-show="showLoading">'
					+ '<p>Loading...</p>'
					+ '<p><img src="images/loading.gif"></p>'
				+ '</div>'
			+ '</div>',
		scope: {
			showLoading : "=",
		},
		link: function(scope, element, attributes) {
			// todo
			scope.showLoading = true;
		}
	}
});


})();