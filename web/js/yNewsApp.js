"use strict"; // More verbose error/notification reporting...

(function(){

// moved to global var... 
//var API_URL = "../api/index.php/";

// Used for FB Like button 
//var FB_APP_ID = "1507979672750288"; 

var app = angular.module("yNewsApp", ["ngRoute", "ngSanitize", "angulike", "starRating", "btLoading"]);

app.config(function($routeProvider){
	$routeProvider.when('/category/:category?/:pageNum?', {
		templateUrl : 'categoryView.html',
		controller : "articleListController",
	});
	$routeProvider.when('/search/:searchTerm?/:pageNum?', {
		templateUrl : 'categoryView.html',
		controller : "articleListController",
	});
	$routeProvider.when('/contact', {
		templateUrl : 'contactView.html',
		controller : "contactController",
	});
	$routeProvider.when('/about', {
		templateUrl : 'aboutView.html',
		controller : "aboutController",
	});
	$routeProvider.otherwise({redirectTo : '/category'});
});

// Initialize app
app.run( function($rootScope, $location, $window, $routeParams){
	
	$rootScope.facebookAppId = FB_APP_ID;
	$rootScope.rootShowLoading = false;
	$rootScope.searchTerm = $routeParams.searchTerm;

	$rootScope.articleSearch = function(){
		$location.path("/search/" + $rootScope.searchTerm);
	}

	$rootScope.isCategory = function (cat) {
		return $rootScope.category == cat;
    };

    $rootScope.isPage = function(page){
    	return $location.path() == "/" + page;
    }
});


app.controller('articleListController', function($scope, $rootScope, $http, $routeParams, $location, $window){

	$rootScope.category = $routeParams.category ? $routeParams.category : "all";
	if($routeParams.searchTerm){
		$rootScope.category = "search";
	}

	$rootScope.pageTitle = $rootScope.category == "all" 
		? "Latest news" 
		: ( $rootScope.category.charAt(0).toUpperCase() + $rootScope.category.slice(1) );


	var pageNum = $routeParams.pageNum;

	if( ! pageNum || pageNum < 1){
		pageNum = 1;
	}

	$scope.pageNum = pageNum;


	//$rootScope.reloadFbScript();

	$rootScope.rootShowLoading = true;
	var queryPromise = null;
	if($rootScope.category == "search"){
		queryPromise = $http.get(API_URL + "article/list/search/"+$scope.pageNum+"?search_term="+$routeParams.searchTerm);
	} else {
		queryPromise = $http.get(API_URL + "article/list/" + $rootScope.category + "/" + $scope.pageNum);
	}
	queryPromise.then(function(resp){
		$scope.articles = resp.data.articles;
		$scope.totalPages = resp.data.total_pages;
		$rootScope.rootShowLoading = false;
		reloadPagination();

		// Run after render is finished...
		setTimeout(function () { 
				// BInd event that will show a tooltip once a text within description is selected
				document.bindSelectionTooltipEvent(".description");
		}, 500);



	});


	var reloadPagination = function(){
		var isMoreThanTen = false;
		var output = [];
		var i = 0;
		var pageOffset = 5;
		for(i = 0; i < $scope.totalPages; i++){


			// If there are too many pages, print only 10 pages around current:
			if($scope.totalPages > 10){
				if(i >= parseInt($scope.pageNum) - (pageOffset+1) && i < parseInt($scope.pageNum) + pageOffset){
					output.push(i+1);
				}
				isMoreThanTen = true;
			} else {
				output.push(i+1);
			}
		}

		// Show user that we have more pages than displayed; i.e. show last page in the pagination:
		if(isMoreThanTen && $scope.pageNum < $scope.totalPages - pageOffset){
			output.push("...");
			output.push($scope.totalPages);
		}
		$scope.pages = output;
	}

	$scope.nextPage = function(direction){
		
		var newPageNum = -1;
		if(direction == -1){
			newPageNum = parseInt($scope.pageNum) - 1;
		} else {
			newPageNum = parseInt($scope.pageNum) + 1;
		}

		// We need to make sure page is not out of bounds
		if(newPageNum > $scope.totalPages || newPageNum < 1){
			return 0;
		} else {
			$scope.pageNum = newPageNum;
		}

		// Everything is okay, so let's move to the next page:
		if($rootScope.category == "search"){
//			$location.path("search/" + $rootScope.category + "/" + $scope.pageNum);
			$location.path("search/" + $routeParams.searchTerm + "/" + $scope.pageNum);
		} else {
			$location.path("category/" + $rootScope.category + "/" + $scope.pageNum);
		}
	}
	
	$scope.pageLink = function(pageNum){
		pageNum = parseInt(pageNum);
		if(! pageNum || pageNum < 0){
			pageNum = $scope.pageNum;
		}
		if($rootScope.category == "search"){
			return "#/search/" + $routeParams.searchTerm + "/" + pageNum;
		}
		return "#/category/" + $rootScope.category + "/" + pageNum;
	}

	$scope.addRating = function(ratingValue, articleId){
		if(ratingValue > 1 && ratingValue <= 5 && articleId > 0){
			$http.get(API_URL + "article/rate/"+articleId+"/"+ratingValue)
				.then(function(res){
//console.log(res.data);
					if(res.data.saved && res.data.saved == 1){
						$window.alert("Thanks for voting!");
					} else if( res.data.message){
						$window.alert(res.data.message);
					} else {
						$window.alert("You can't vote on this article!");
					}
				});
		} else {
			$window.alert("Error");
		}
	}

	$scope.open = function(articleId){
		$window.open( API_URL + "article/open/" + articleId, "_blank");
	}
});

app.controller('contactController', function($scope, $rootScope, $http, $routeParams, $location){
	
	$rootScope.pageTitle = "Contact";

	$scope.sendMessage = function(){
		var pData = { email : $scope.email , message : $scope.message };
		$http.post(API_URL + "contact", pData).then(function(resp){
			alert(resp.data.message);
		});
	}
});

app.controller('aboutController', function($scope, $rootScope, $http, $routeParams, $location){
	
	$rootScope.pageTitle = "About Us";

});


})();