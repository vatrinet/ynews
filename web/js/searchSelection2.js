/**
* This script will automatically search selected (marked with mouse) text and
*   show a tooltip with N results
**/


"use strict"; // verbose error reporting

// Encapsulate script
(function(){

//	var API_URL = "../api/index.php/";

	if(! $.ajax ){
		alert("Please load jQuery library");
	}

	// Public function - bind event to element
	document.bindSelectionTooltipEvent = function(v_jquery_selector){

		$(v_jquery_selector).on('mouseup', function(evt){
//console.log("Event:",evt);
			searchAndShowTooltip(evt);
		});
	}

	// Public function - remove tooltip box
	document.removeTooltipBox = function(){
	    //window.getSelection().removeAllRanges();
	    $("#searchTooltip").remove();
	}

	

	var searchAndShowTooltip = function(evt, selectedText){
	    // Show empty popup and then load content

		//AJAX:
	    var selectedText = window.getSelection().toString();

	    if(selectedText.length < 4){
	    	return false;
	    }

	    $.ajax({
	        url : API_URL + "article/list/search/1?search_term=" + selectedText
	    }).done(function(res){
	        var ajaxResult = $.parseJSON(res);
	        if(ajaxResult.articles){
	            _showTooltip(ajaxResult.articles);
	        }
	    });

	    
	    var _showTooltip = function(jsonArticles){

	        document.removeTooltipBox();

	        if(jsonArticles.length < 1){
	            return false;
	        }

	        var tooltipHtml = "<h3>Relevant articles</h3>"
	            + "<ul>";

	        for(let i = 0; i < jsonArticles.length; i++){

	            var row = jsonArticles[i];
	            tooltipHtml += "<li><a href='" + row.url + "' target='_blank'>"+row.title+"</a></li>";
	        }
	        tooltipHtml += "</ul>"
	            + "<p><button onclick='removeTooltipBox()'>Close</button></p>";

	        var tooltipElement = document.createElement("div");

	        tooltipElement.id = "searchTooltip";
	        tooltipElement.className = "search-tooltip"
	        tooltipElement.style.position = "absolute";
	        tooltipElement.style.top = (evt.pageY - 0) + "px";
	        tooltipElement.style.left = evt.pageX + "px";
	        tooltipElement.innerHTML = tooltipHtml;

	        document.body.appendChild(tooltipElement);
	    };
	}

})();