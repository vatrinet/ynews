"use strict";

/**
* This script will automatically search selected (marked with mouse) text and
*   show a tooltip with N results
**/




var searchSelection = {

    searchAndShowTooltip : function(evt, selectedText){

//function searchAndShowTooltip(evt,selectedText){
            //AJAX:
        $.ajax({
            url : "../api/index.php/article/list/search/1?search_term="+selectedText
        }).done(function(res){
            var ajaxResult = $.parseJSON(res);
            if(ajaxResult.articles){
                _showTooltip(ajaxResult.articles);
            }
        });            

        var _showTooltip = function(jsonArticles){

            searchSelection.removeTooltipBox();

            if(jsonArticles.length < 1){
                return false;
            }

            var tooltipHtml = "<h3>Suggested articles</h3>"
                + "<ul>";

            for(let i = 0; i < jsonArticles.length; i++){

                var row = jsonArticles[i];
                tooltipHtml += "<li><a href='" + row.url + "' target='_blank'>"+row.title+"</a></li>";
            }
            tooltipHtml += "</ul>"
                + "<p><button onclick='searchSelection.removeTooltipBox()'>Close</button></p>";

            var tooltipElement = document.createElement("div");

            tooltipElement.id = "searchTooltip";
            tooltipElement.className = "search-tooltip"
            tooltipElement.style.position = "absolute";
            tooltipElement.style.top = (evt.pageY - 20) + "px";
            tooltipElement.style.left = evt.pageX + "px";
            tooltipElement.innerHTML = tooltipHtml;

            document.body.appendChild(tooltipElement);
        };
    }
};

searchSelection.bindEvent = function(evt){
    $(document).on("mouseup", function(evt){
        var isDescriptionSelected = false;

        for(let i = 0; i < evt.yNewsGetClickPath().length; i++){

    //    for(let i = 0; i < evt.originalEvent.path.length; i++){
            var el = evt.originalEvent.path[i];
            if(el.getAttribute && el.getAttribute("class") == "description"){
                isDescriptionSelected = true;
            }
        }
        var selectedText = window.getSelection().toString();
        if(isDescriptionSelected && selectedText.length >= 4){
            searchSelection.searchAndShowTooltip(evt,selectedText);
        }
    });
};
searchSelection.removeTooltipBox = function(){
    window.getSelection().removeAllRanges();
// Remove tooltip element so we can create it again later
    $("#searchTooltip").remove();
};

$(document).on("click", function(){
    // Remove selection so that any click close the tooltip popup
   // searchSelection.removeTooltipBox();
});

searchSelection.bindEvent();

// Perform search on a text selection within description
$(document).on("mouseup", function(evt){
   // searchSelection.bindEvent(evt);    
});