<?php

class MY_Model extends CI_Model{
	
	protected $table_name = "";

	protected $fields = array();
	protected $join_fields = array();
	/* Example:
	protected $join_fields = array(
		'product_name' => array('products', 'name', 'id', 'product_id'),
		'variant_name' => array('group_variants', 'variant_name', 'id', 'product_variant_id'),
	);
	*/

// TODO:
	protected $validate = true;
	protected $validations = array();
	/* Example:
	protected $validations = array(
		array('name', 'Name','required|alpha_numeric|min_length[3]'),
		array('product_id', 'Product','required|numberic|greater_than[1]'),
	);
	/**/
	// Limit for find query start:limit format
	protected $limit = array(0,100);

	public function __construct(){
		if( $this->table_name == "" || !count($this->fields) ){
			throw new Exception('Please set $table_name and $fields properties!', 1);
		}
		parent::__construct();
	}

	private function _run_join(){

		if(count($this->join_fields) > 0){
		

			// Avoid "ambiguous column" SQL error...
			$joined_tables = array();

			// Select SQL 
			$select_sql = $this->table_name . '.*';
			foreach ($this->join_fields as $fld => $join_field){

				$select_sql .= " , " . $join_field[0] . '.' . $join_field[1] . " as $fld ";
				if( in_array($join_field[0], $joined_tables)){
					continue;
				}
				// Generate join SQL string:
				$join_sql = $join_field[0] . '.' . $join_field[2] 
					. ' = '
					. $this->table_name . '.'.$join_field[3];

//echo $join_sql . '<hr>';				
				$this->db->join($join_field[0], $join_sql, 'left');

				$joined_tables[] = $join_field[0];

				// Add the field to select part:
				

			}
//echo $select_sql. '<hr>';				
			$this->db->select($select_sql);
		}
	}

	public function get($id = 0, $where = null){

//add join query
		$this->_run_join();
//var_dump($id);
		if($where != null){
			$this->db->where($where);
		}
		if($id > 0){
			$this->db->where($this->table_name.'.id', $id);
		}
		$row = $this->db->get($this->table_name)->row_array();
//echo '<pre>';
//echo $this->db->last_query();
//echo '</pre>';
		if(method_exists($this, "afterGet")){
			$row = $this->afterGet($row);
		}
		return $row;
	}

	/**
	* @param array $where Array of field>value pairs
	* @return array
	**/ 
	public function find($where = array()){
		$this->_run_join();
		$this->db->limit($this->limit[0], $this->limit[1]);
		$result = $this->db->get($this->table_name)->result_array();
		if(method_exists($this, "afterFind")){
			$result = $this->afterFind($result);
		}
		return $result;
	}

	public function save($id, $post){

		$validate_all = $id < 1;
		$validation_result = $this->run_validation($post, $validate_all);
		if( $validation_result != ""){
			return $validation_result;
		}

		if(method_exists($this, "beforeSave")){
			$this->beforeSave($data);
		}

		$data = array();
		foreach ($this->fields as $fld) {
			if(isset($post[$fld])){
				$data[$fld] = $post[$fld];
			}
		}

		if($id > 0){
			$ok = $this->db->where('id', $id)->update($this->table_name, $data);
		} else {
			$ok = $this->db->insert($this->table_name, $data);
			$id = $this->db->insert_id();
		}

		if(!$ok){
			return false;
		}

		$post['id'] = $id;

		if(method_exists($this, "afterSave")){
			$this->afterSave($post['id'], $post);
		}

		return $post;
	}
	/**
	* 
	Use these if you want to filter or manipulate data before saving.
	Just create beforeSave($post_data) in your model.
	These will be run automatically if you use get() from this class
	* @param array $post Input (usually POST) data.
	**/
#	public abstract function beforeSave($post);

	/**
	* See beforeSave. Run after save with $id parameter.
	* @see beforeSave()
	* @param int $id ID Returned by save() function.
	* @param array $post Input (usually POST) data.
	**/
#	public abstract function afterSave($id, $post);

	/**
	* This is being run automatically after get() is called.
	* @param array $row Output from get()
	* 
	* @return array Should return extended / filtered data from input.
	**/
#	public abstract function afterGet($row);

	/** 
	* Run automaticaly if validation is on
	* @param array $data Data to validate!
	* @param boolean $validate_all To validate all (required) fields or not.
	*
	* @return string Empty string ("") or validation errors.
	**/
	private function run_validation($post, $validate_all = false){
		
		if($this->validate != true){
			return "";
		}

		$this->load->library('form_validation');

		$data = array();
		foreach ($this->fields as $fld) {
			if(isset($post[$fld])){
				$data[$fld] = $post[$fld];
			}
		}
		$this->form_validation->set_data($data);

		$check_fields = $validate_all==true ? $this->validations : $data; 

		foreach ($this->validations as $validation) {
			if(! $validate_all && ! isset($data[$validation[0]] )){
				continue;
			}
			$this->form_validation->set_rules($validation[0],$validation[1],$validation[2]);
		}

        if ($this->form_validation->run() == FALSE){
        	return validation_errors("-", "\n
");
	    } else {
			return "";
		}
	}

}