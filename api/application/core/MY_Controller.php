<?php

// TODO: - what if not Apache (apache_get_headers() )?
//		 - cache check auth (1-2h ?)
class MY_Controller extends CI_Controller
{

	/**
	* REST data that is sent in HTTP body instead in POST variables.
	**/ 
	protected $payload_data = array();

	public function __construct()
	{

		session_start();
		//sleep(1.5);
		parent::__construct();

		$post = json_decode(file_get_contents('php://input'));
		$this->payload_data = (array) $post;

		$this->load->driver('cache', array(
			'adapter' => 'apc',
			'backup' => 'file'
		));

//		$this->output->enable_profiler(TRUE);
	}

	/**
	* Call this for output - always!
	**/
	public function json_output($data){
		echo json_encode($data);
	}
}