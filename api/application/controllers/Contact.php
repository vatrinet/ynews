<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Contact extends MY_Controller {

	public function index(){
		$this->load->library("form_validation");

		$this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
		$this->form_validation->set_rules("message", "Message", "trim|required|min_length[5]");

		$data = array(
			"email"   => isset($this->payload_data["email"]) || "",
			"message" => isset($this->payload_data["message"]) || "",
		);
		$this->form_validation->set_data($data);

		if ($this->form_validation->run() == FALSE){
			$this->json_output(array(
				"success" => 0,
				"message" => validation_errors(" ", " "),
			));
		} else{
			$this->json_output(array(
				"success" =>1,
				"message" => "Message sent!"
			));
		}
	}

}//class