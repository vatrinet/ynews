<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scraper extends MY_Controller {

	public function scrape_all(){
		$result = $this->db->get(TBL_ARTICLE_CATEGORIES)->result_array();
		foreach ($result as $row) {
			$this->scrape_category($row['slug']);
		}
	}

	public function scrape_category($category = ""){
		$this->cache->clean();
		$this->load->model("Scraper_model");
		$this->Scraper_model->scrape_category($category);
	}

}