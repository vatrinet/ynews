<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends MY_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('Article_model');
	}

	/** 
	* @param string $category Category slug
	**/
	public function list($category = "", $page = 1){

		if($this->input->get("search_term")){
			$category = "search";
		}
		$result['articles'] = $this->Article_model->get_articles($category, $page);
		$result['total_pages'] = $this->Article_model->count_pages($category);
		$this->json_output($result);
	}

	public function rate($id, $rating){
		$rated = $this->Article_model->rate_article($id, $rating);
		if($rated === true){
			$this->json_output(array('saved' => 1));
		} else{
			$this->json_output(array('saved' => 0, 'message' => $rated));
		}
	}

	public function open($id){
		$id = intval($id);
		if($id < 1){
			echo "Error.";
			return false;
		}
		$row = $this->db->where('id', $id)->get(TBL_ARTICLES)->row();
		if( ! isset($_SESSION['open-'.$id])){
			$this->db->query("UPDATE " . TBL_ARTICLES . " SET `views` = (views + 1) WHERE id=?", $id);
		}
		$url = isset($row->url) ? $row->url : "";
//var_dump($row);
		if($url == ""){
			echo "An unknown error occured. Please contact website administrator.";
			return false;
		}
		$_SESSION['open-'.$id] = true;
		header("Location: $url");
		//echo $url;
	}

	public function clear_cacheee(){ var_dump($this->cache->clean()); }

}//class