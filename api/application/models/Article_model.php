<?php

class Article_model extends CI_Model{

	// Cache ID template
	private $cache_id_template_category = "%s-%s";

	public function get_articles($category, $page){

		//$this->load->driver

		$page = (int) $page;
		//$page--;
		if($page < 1){
			$page = 1;
		}

		if($category == "search"){
			return $this->_search($page);
		}

		$per_page = PER_PAGE;
		$sql_limit_start = ($page-1) * $per_page;
		
		$category = $this->db->escape_str($category);

		//$cache_id = "$category-$page";
		$cache_id = sprintf($this->cache_id_template_category,$category, $page);

		$cache_value = $this->cache->get($cache_id);
//$cache_value=false;
		if( $cache_value ){
			return $cache_value;
		}

		$sql_join = "";
		if($this->category_exist($category)){
			$sql_join = " JOIN " . TBL_ARTICLE_CATEGORIES . " c 
				ON c.slug = '$category' and c.id = a.category_id
			";
		}

		$qstr = "SELECT a.* , (select AVG(rating) FROM ".TBL_ARTICLE_RATINGS." where article_id = a.id) as rating
			FROM " . TBL_ARTICLES . " a 
			$sql_join
			group by a.id
			ORDER BY a.date DESC
			LIMIT $sql_limit_start , $per_page
		";
		$result = $this->db->query($qstr)->result_array();

		$this->add_user_rating_field($result);

		$this->cache->save($cache_id, $result, CACHE_TIMEOUT);

		return $result;
	}


	/** 
	* @todo: full-search index
	**/
	private function _search($page){
		$search_term = $this->input->get("search_term");
		$search_term = $this->db->escape_str($search_term);

//		$cache_id = "search-" . md5($search_term) . "-$page";

		$per_page = PER_PAGE;
		$sql_limit_start = ($page-1) * $per_page;


		$qstr = "
			SELECT a.* ,  (select AVG(`rating`) FROM " . TBL_ARTICLE_RATINGS . " WHERE article_id = a.id) AS `rating`
			FROM ".TBL_ARTICLES." a
			WHERE MATCH(a.title,a.description) AGAINST('$search_term')
			LIMIT $sql_limit_start , $per_page
		";

		$result = $this->db->query($qstr)->result_array();

		$this->add_user_rating_field($result);

//		$this->cache->save($cache_id, $result, CACHE_TIMEOUT);

		return $result;
	}

	// This function adds rating for each article in result set for current visitor
	private function add_user_rating_field(&$result){
		foreach ($result as &$row){
			$cookie_id = "rate-" . $row['id'];
			if( isset($_COOKIE[$cookie_id])){
				$row['my_rating'] = $_COOKIE[$cookie_id];
			} elseif( isset($_SESSION[$cookie_id])){
				$row['my_rating'] = $_SESSION[$cookie_id];
			} else {
				$row['my_rating'] = 0;
			}

//todo? 
//			$row['rating'] = round($row['rating']);
		}
	}

	public function count_pages($category){

		$search_term = $this->input->get("search_term");
		$search_term = $this->db->escape_str($search_term);

		$category = $this->db->escape_str($category);

		$cache_id = "category_count-" . $category . ( $search_term ? "-" . md5($search_term) : "");
		$cache_value = $this->cache->get($cache_id);
		if( $cache_value ){
			return $cache_value;
		}

		$qstr = "SELECT count(a.id) cnt
			FROM ".TBL_ARTICLES . " a ";

		if($category == "search"){
			// Fulltext search
			$qstr .= " WHERE MATCH(a.title,a.description) AGAINST('$search_term') ";
		}

		if($this->category_exist($category)){
			$qstr .= " JOIN " . TBL_ARTICLE_CATEGORIES . " c 
				ON c.slug = '$category' and c.id = a.category_id
			";
		}

		$articles_cnt = $this->db->query($qstr)->row()->cnt;

		$pages_num = ceil($articles_cnt / PER_PAGE);

		$this->cache->save($cache_id, $pages_num, CACHE_TIMEOUT);

		return $pages_num;
	}

	public function category_exist($category){

		// TODO cache categories

		if( strval($category) == ""){
			return false;
		}

		$exists = false;
		$found = $this->db->where('slug', $category)->get(TBL_ARTICLE_CATEGORIES)->num_rows();
		
		return $found > 0;
	}

	public function get_category_id_by_slug($category){
		$category = $this->db->escape_str($category);
		$row = $this->db->where('slug', $category)->get(TBL_ARTICLE_CATEGORIES)->row();
		return isset($row->id) ? $row->id : 0;
	}

	public function rate_article($id, $rating){

		$cookie_id = "rate-" . $id;

		$rating = intval($rating);

		if( 
			$this->get_my_vote($id) > 0 
			|| $rating < 1
			|| $rating > 5
		){
			return "You already voted for this article!";
		}

		$saved = $this->db->insert(TBL_ARTICLE_RATINGS, array(
			'article_id' => $id,
			'user_ip'    => $_SERVER['REMOTE_ADDR'],
			'rating'     => intval($rating),
		));

		if($saved){
			setcookie($cookie_id, $rating, time()+60*60*24*30*100, "/");
			$_SESSION[$cookie_id] = $rating;
			$this->clear_article_cache($id);
		}
		return (boolean)$saved;
	}

	public function get_my_vote($article_id){
		$cookie_id = "rate-" . $article_id;
		if( isset($_COOKIE[$cookie_id])){
			return $_COOKIE[$cookie_id];
		} elseif( isset($_SESSION[$cookie_id])){
			return $_SESSION[$cookie_id];
		} else {
			return 0;
		}
	}

	public function clear_article_cache($id){
		$row = $this->db->query("
            SELECT c.id as cat_id, c.slug as cat_slug 
            	FROM ".TBL_ARTICLES." a
            	JOIN ".TBL_ARTICLE_CATEGORIES." c ON c.id = a.category_id
            	WHERE a.id = ?", $id)
			->row();
log_message("error", "Deleting cache for article $id ...");

		if(isset($row->cat_id)){
			$i = 0;
			while($i <= $this->count_pages($row->cat_slug) ){
				$i++;

				$page = $i;
				$cache_id = sprintf($this->cache_id_template_category, $row->cat_slug, $page);
				$deleted = $this->cache->delete($cache_id);

				log_message("error", "Cache $cache_id is deleted=".$deleted);
			}
		}
	}


}//class