<?php

class Scraper_model extends CI_Model{

	public function scrape_category($category){
		$this->load->model("Article_model");

		if( ! $this->Article_model->category_exist($category)){
			return false;
		}

		$url = "http://news.yahoo.com/rss/" . $category;

		$xml = simplexml_load_file($url);

		if(! isset($xml->channel->item)){
			// todo: log error 
			return false;
		}

//print_r($xml);

		$all_urls = array();
		foreach ($xml->channel->item as $item) {
			if( isset($item->link)){
				$all_urls[] = $item->link;
			}
		}

		$insert_urls = $this->exclude_existing_items($all_urls);

		$category_id = $this->Article_model->get_category_id_by_slug($category);

		foreach ($xml->channel->item as $item) {
			if( in_array($item->link, $insert_urls)){
				$this->db->insert(TBL_ARTICLES, array(
					"title"       => $item->title,
					"url"         => $item->link,
					"description" => $item->description,
					"date"        => date("Y-m-d H:i:s", strtotime($item->pubDate)), 
					"category_id" => $category_id,
				));
			}
		}
	}

	private function exclude_existing_items($all_urls){
		
		$result =  $this->db->where_in("url", $all_urls)->get(TBL_ARTICLES)->result_array();

		$output = array();

		$existing_urls = array();
		foreach ($result as $row) {
			$existing_urls[] = $row['url'];
		}

		foreach ($all_urls as $url) {
			if( !in_array($url, $existing_urls)){
				$output[] = $url;
			}
		}
		return $output;
	}


}