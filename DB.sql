create table articles(
	id int primary key auto_increment, 
	title varchar(255) not null,
	url varchar(255) not null,
	description text not null,
	`created` datetime default current_timestamp,
	date datetime not null,
	category_id int not null
) ENGINE='MyISAM' COLLATE 'utf8_general_ci';


create table article_categories(
	id int primary key auto_increment, 
	slug varchar(255) not null,
	title varchar(255) not null,
	`created` datetime default current_timestamp
) ENGINE='MyISAM' COLLATE 'utf8_general_ci';


INSERT INTO `article_categories` (`slug`, `title`) 
VALUES 
('science', 'Science'),
('tech', 'Tech'),
('world', 'World'),
('politics', 'Politics'),
('health', 'Health');


ALTER TABLE `articles` ADD INDEX `url` (`url`);

create table articlesbkp select * from articles;
ALTER TABLE articles ADD FULLTEXT(`title`,`description`);

create table article_ratings(
	id int primary key auto_increment,
	article_id int not null,
	user_ip varchar(100) not null,
	rating enum("1","2","3","4","5") not null,
	`created` datetime default current_timestamp
) ENGINE='MyISAM';


# Doesn't work for MyISAM tables; and will never work...
#     alter table articles add foreign key (category_id ) references article_categories(id);
#     alter table article_ratings add foreign key (article_id) references articles(id);